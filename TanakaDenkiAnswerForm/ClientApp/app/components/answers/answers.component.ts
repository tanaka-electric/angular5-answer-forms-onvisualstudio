﻿import { Component, OnInit, ViewChild } from '@angular/core'
import { NgForm } from '@angular/forms';
import * as Material from '@angular/material';
// azure function用
import { HttpClient } from '@angular/common/http';

class Answer {
    public id: number;
    public label: string;
};

@Component({
    selector: 'app-answers',
    templateUrl: './answers.component.html',
    styleUrls: ['./answers.component.css']
})
export class AnswersComponent {

    answers: Answer[] = [
        { id: 1, label: '購入する' },
        { id: 2, label: '購入しない' },
        { id: 3, label: 'どちらともいえない' }
    ];

    @ViewChild('tanakaForm')
    private tanakaForm: NgForm;

    constructor(private http: HttpClient, private mdSnackBar: Material.MatSnackBar) {
    }

    // htmlテンプレートの(ngSubmit)="register(tanakaForm)" で呼ばれるアンケート登録ロジック
    register(tanakaForm: NgForm) {

        // ここでtanakaForm.value | json の answer（回答番号） をazure functionに通達する。
        const functionURI = 'https://telc-answer-func.azurewebsites.net/api/HttpTriggerJS1?code=wbHGua6bi6dWlAMpci7bWKHMMo2LOmHyynz9Eef6vFGmaow2tG6sQQ==';

        this.http.post(functionURI, tanakaForm.value).subscribe(data => {
            // クラウドへの通達結果をコンソールに表示
            console.log("azureへの通達結果-> " + data);

            // ユーザへのフィードバックとしてtoastを表示
            this.mdSnackBar.open(
                'ご回答ありがとうございます',
                'OK',
                {
                    'duration': 2000		// 表示時間(ms)
                }
            );
        });
    }

}