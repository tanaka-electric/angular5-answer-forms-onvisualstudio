import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import * as Material from '@angular/material';

import { AppComponent } from './components/app/app.component';
import { AnswersComponent } from './components/answers/answers.component';

import { BrowserModule } from '@angular/platform-browser';
// for toast
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService } from 'angular2-toaster';



@NgModule({
    declarations: [
        AppComponent,
        AnswersComponent,
    ],
    providers: [
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        ToasterModule,
        Material.MatButtonModule,
        Material.MatMenuModule,
        Material.MatToolbarModule,
        Material.MatIconModule,
        Material.MatCardModule,
        Material.MatSnackBarModule,
        Material.MatRadioModule,
    ]
})
export class AppModuleShared {
}
