## これは何？

- VisualStudio ASP.net Core with Angular5 + Materialベースのアンケートアプリです。
- 別ソリューションの angular4-answer-forms のコンポーネントを上記骨格に配置しなおしました。
- 集計結果の保存用に別途azure functionのHttpTriggerスクリプトが実行中であることが条件となっています。詳細は angular4-answer-forms のreadmeを参照ください。

## ビルド＆実行するには

Ctl+F5 で自動的にパッケージのインストール＆webpack＆起動まで一気に実行されます。
※ローカルでも本番のazure functionのエンドポイントを利用しますので、ローカル実行と本番実行で集計結果が混じることを留意してください。

## デプロイ手順

### azure app service へ配置
プロジェクト右クリック -> 発行 でazure app serviceへ直接発行します。

- Azure App Service名：telc-answer
- URL: https://telc-answer.azurewebsites.net

### アンケ―トの集計

- Azure Storage Explorerで以下のテーブルを参照
- Strage Accounts -> (Azure Function名) -> Tables -> outTable 
- エクスポートする
- wish列を集計する
- ※　AzureWebJobsHostLogs(yyyymm)のLogOutput列には生のjsonも格納している。

## 本番URL
https://telc-answer.azurewebsites.net
